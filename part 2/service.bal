import ballerina/graphql;

public type CovidStat record{
    string date;
    string region;
    int deaths;
    int confirmed_cases;
    int recoveries;
    int tested;
};

table<CovidStat> covidStatTable = table[
    {date: "12/09/2021", region: "Khomas", deaths: 39, confirmed_cases: 465, recoveries: 67, tested: 1200},
    {date: "12/09/2021", region: "Caprivi", deaths: 0, confirmed_cases: 0, recoveries: 0, tested: 0},
    {date: "12/09/2021", region: "Erongo", deaths: 0, confirmed_cases: 0, recoveries: 0, tested: 0},
    {date: "12/09/2021", region: "Hardap", deaths: 0, confirmed_cases: 0, recoveries: 0, tested: 0},
    {date: "12/09/2021", region: "Karas", deaths: 39, confirmed_cases: 465, recoveries: 67, tested: 1200},
    {date: "12/09/2021", region: "Kunene", deaths: 0, confirmed_cases: 0, recoveries: 0, tested: 0},
    {date: "12/09/2021", region: "Ohangwena", deaths: 0, confirmed_cases: 0, recoveries: 0, tested: 0},
    {date: "12/09/2021", region: "Omaheke", deaths: 0, confirmed_cases: 0, recoveries: 0, tested: 0},
    {date: "12/09/2021", region: "Omusati", deaths: 39, confirmed_cases: 465, recoveries: 67, tested: 1200},
    {date: "12/09/2021", region: "Oshana", deaths: 0, confirmed_cases: 0, recoveries: 0, tested: 0},
    {date: "12/09/2021", region: "Oshikoto", deaths: 0, confirmed_cases: 0, recoveries: 0, tested: 0},
    {date: "12/09/2021", region: "Otjozondjupa", deaths: 0, confirmed_cases: 0, recoveries: 0, tested: 0}
];

service /graphql on new graphql:Listener(4000) {

    resource function get stats () returns stat [] {
        CovidStat[] covidStats = covidStatTable.toArray().cloneReadOnly();
    return covidStats.map(entry => new stat(entry));   
    }

    

}

public service class stat {
    private string date;
    private string region;
    private int deaths;
    private int confirmed_cases;
    private int recoveries;
    private int tested;

    private final readonly & CovidStat CStat;

    function init(CovidStat CStat) {
        self.CStat = CStat.cloneReadOnly();
       // string date, string region, int deaths, int confirmed_cases, int recoveries, int tested
        //self.date = date;
        //self.region = region;
        //self.deaths = deaths;
        //self.confirmed_cases = confirmed_cases;
        //self.recoveries = recoveries;
        //self.tested = tested;

    }

    resource function get date() returns string {
        return self.CStat.date;

    }

    resource function get region() returns string {
        return self.CStat.region;

    }

    resource function get deaths() returns int {
        return self.CStat.deaths;

    }

    resource function get confirmed_cases() returns int {
        return self.CStat.confirmed_cases;

    }

    resource function get recoveries() returns int {
        return self.CStat.recoveries;

    }

    resource function get tested() returns int {
        return self.CStat.tested;

    }
    
    function setDate(string date) {
        self.date = date;

    }

     function setRegion(string region) {
        self.region = region;

    }

    function setDeaths(int deaths) {
      self.deaths=deaths;
    }

    function setConfirmed_cases(int confirmed_cases) {
      self.confirmed_cases=confirmed_cases;
    }

    function setRecoveries(int recoveries) {
      self.recoveries=recoveries;
    }

    function setTested(int tested) {
      self.tested=tested;
    }

}

