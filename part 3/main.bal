
import ballerina/http;
import ballerina/io;

public type course record {|
string code?;
string weight?;
string marks?;
|};

public type UserDetail record {|
string number?;
string name?;
string email?;
course[] courses = [];
|};
UserDetail[] all_users = [];

listener http:Listener httpListener = new(9090);

service /users on new http:Listener(8080) {

   // retrives all users
    resource function get all() returns UserDetail[] {
    io:println("handling GET request to /users/all");
    return all_users;
    }

    //Adds a new user to all_users array
    resource function post insert(@http:Payload UserDetail new_user) returns json {
    io:println("handling POST request to /users/insert");
    all_users.push(new_user);
    return {done: "Ok"};
    }

    // returns specific user
    resource function get users/[string  number]() returns UserDetail|http:Response {
      UserDetail thatUser={};
           foreach var item in all_users {
        if(item.number==number){
         thatUser=item;
        }
        
     }
     return thatUser;
  
    }

    // Supposed to update user info
    resource function put users/[string  number](@http:Payload UserDetail payload) returns UserDetail|http:Response {
        UserDetail thatUser={};
           foreach var item in all_users {
        if(item.number==number){
         all_users.push();
         thatUser=item;
        }
        
     }
     return thatUser;
   
    }

    // reomves user for all_users array
    resource function delete users/[string  number]() returns json {
           foreach var item in all_users {
        if(item.number==number){
         item.removeAll();
          return {done: "Ok"};
        }  
     }
    }

   //supposed update user's course
    resource function updateCourse users/course/[string code] (@http:Payload UserDetail payload) {
      foreach var item in all_users {
         if (item.number == payload.number){
            foreach var  items in item.courses{
               if (items.code == code){
                  
               }
            }
         }
      }
      
    }
}