import ballerina/grpc;
import ballerina/io;
import ballerina/lang.value as value1;
import ballerina/log;

//object classes begin
public class learners {
    string id;
    string[] b;

    function init(string id, string[] b) {
        self.id = id;
        self.b = b;
    }
}

public class courses {
    string id;
    assignments[] assign;
    string assessor;

    function init(string id, assignments[] assign, string assessor) {
        self.id = id;
        self.assign = assign;
        self.assessor = assessor;
    }
}

public class assignments {
    string name;
    string weight;
    string result;

    function init(string name, string weight, string result) {
        self.name = name;
        self.weight = weight;
        self.result = result;
    }
}
//object classes end

//records for remote method use begin
public type userReg record{
    string sid;
    string cid;
};

type mathRequest record{
    string id ;
    int num2 ;
	string assessr ;
};

public type assiWeight record{
    string Aid;
    string AWeight;
};

public type assessr record{
    string Cid;
    string Aid;
};
// records for remote method use ends

//creation and initialising of learner course list arrays
string[] boba = ["first", "second", "last"];
string[] pata = ["first", "second", "last"];
string[] sanda = ["first", "second", "last"];

//creation and initialising of learners
learners bob = new learners("bob", boba);
learners patrick = new learners("patrick", pata);
learners sandy = new learners("sandy", sanda);

//creation and initialising of assignment arrays
assignments[] bi = [a, b, c];
assignments[] ma = [d, e, f];
assignments[] ph = [g, h, i];
 
//creation and initialising of course objects
courses biology = new courses("biology", bi, "name");
courses math = new courses("math", ma,"name");
courses physics = new courses("physics", ph,"name");

//creation and initialising of assignments
assignments a = new assignments("assi1","30%","undefined");
assignments b = new assignments("assi2","30%","undefined");
assignments c = new assignments("assi3","30%","undefined");
assignments d = new assignments("assi4","30%","undefined");
assignments e = new assignments("assi5","30%","undefined");
assignments f = new assignments("assi6","30%","undefined");
assignments g = new assignments("assi7","30%","undefined");
assignments h = new assignments("assi8","30%","undefined");
assignments i = new assignments("assi9","30%","undefined");

learners[] stude = [bob, patrick, sandy];
string[] admin = ["joe", "bart", "steve"];
string[] assessor = ["justin", "yoongi", "jungkook"];
courses[] course = [biology, math, physics];
assignments[] assig = [a, b, c, d, e, f, g, h, i];


@grpc:ServiceDescriptor {
   descriptor: ROOT_DESCRIPTOR_PART1,
    descMap: getDescriptorMapPart1()
}
service "HelloWorld" on new grpc:Listener(9090) {

    remote function register_course(userReg na) returns string  {
        int n = 0;

        while n < course.length() {
            if(course[n].id == na.cid){
                return "Student" + na.sid;
            }
            else{
                
            }
        n += 1;
        }   
        return "Student" + na.sid;
    }

    remote function submit_assignments(assignments na) {
        assig.push(na);
    }

    remote function check_result(userReg na) returns string {

         int n = 0;

        while n < bi.length() {
            if(bi[n].name == na.cid){
                return "Student" + na.sid;
            }
            else{
                io:println("That user does not exist");
            }
        n += 1;
        
        }   
        return "Student" + na.sid;
    }

        remote function create_courses
        (Create_coursesStreamingClient caller, stream<UserRequest, grpc:Error?> clientStream) returns error? {

        check clientStream.forEach(function(UserRequest value) {

            log:printInfo("Received:", value=value);

            int n = 0;

            while n < course.length() {
                if(course[n].id == value.id1){
                    checkpanic caller->sendUserRequest(value);
                }
                else{
                    assignments[3] assignm = [a, b, c];
                    courses newc = new courses(value.id1, assignm, value.assessor3);
                    course.push(newc);
                }
            n += 1;
            
            }    
            
        });
        check caller->complete();

    }

    remote function SetAssiWeight(assiWeight na) returns string {
         int n = 0;

        while n < bi.length() {
            if(bi[n].name == na.Aid){
                bi[n].weight = na.AWeight;
                return "Result" + bi[n].weight;
            }
            else{
                io:println("That user does not exist");
            }
        n += 1;
        
        }   
       return "Result" + bi[n].weight;
        
    }

    remote function assign_courses(assessr na) {
        int n = 0;

        while n < course.length() {
            if(course[n].id == na.Cid){
                course[n].assessor = na.Aid;
            }
            else{
                
            }
        n += 1;
        }   
        
    }

    remote function request_assignments() {
        
    }

    remote function submit_marks(string na) {
        int n = 0;

        while n < assig.length() {
            if(assig[n].name == na){

            }
            else{

            }
        n += 1;
        
        }     
        
    }

    remote function create_users() {
    
    }

}