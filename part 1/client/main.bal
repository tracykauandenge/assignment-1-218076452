import ballerina/io;

// Creates a gRPC client to interact with the remote server.
HelloWorldClient ep = check new ("http://localhost:9090");

public type learner record {
    string id;
    course[] b?;
};

public type course record {
    string id;
    assignment[] assign;
    string assessor?;
};

public type assignment record {
    string name;
    string weight;
    string result?;
};

public function main1() returns error? {

    io:println("Enter the appropriate digit:");
    io:println("1 - Admin 2 - Learner 3 - Assessor");
    int p=1;

    match p{
        1 => {
             io:println("Please log in: enter ID");
             string a = "bob";
             if (a =="bob"){

             }
             else{
                io:println("That user does not exist");
             }

        }

        2 => {
            io:println("Please log in: enter ID");
        }

        3 => {
            io:println("Please log in: enter ID");
        }
    }


}
